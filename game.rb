require './board.rb'

class Game

  def initialize
    puts "Connect Four (Cuatro en línea)\n\n"
    
    @board = Board.new
    @turn = 1

    @players = []
    print "Player 1, introduce your name: "
    @players[0] = gets.chomp
    print "Player 2, introduce your name: "
    @players[1] = gets.chomp
  end
  
  def game_loop
    round = 1

    while round <= 21
      puts "\nRound #{ round }"
      @board.draw
      play_turn
      play_turn
      round += 1
    end

    puts "Tie!"
  end
  
  def play_turn
    column = -1

    while column == -1
      print "\n#{ @players[@turn - 1] }'s turn [A-G]: "
      column = get_index(gets)

      if column == -1
        puts "Invalid input. Try again."
      end

      if column != -1 && @board.put_chip(column, @turn) == false
        column = -1
      end
    end

    @board.draw
    if @board.check_winner(column) == true
      put_result
    end
    @turn = @turn == 1 ? 2 : 1
  end

  # Get column from player's input
  def get_index(input)
    if input == nil || input.length != 2
      return -1
    end

    input = input.upcase

    # Check if the character is in range [A-G] (ASCII)
    if input[0].ord < 65 || input[0].ord > 71
      return -1
    end
    return input[0].ord - 65
  end

  def put_result
    puts "#{ @players[@turn - 1] } has won!"
    exit(0)
  end

end
