require 'matrix'

class Board

  def initialize
    @cols = 7
    @rows = 6
    @board = Matrix.zero(@rows, @cols)
  end

  def draw
    rows = @rows
    puts "  A B C D E F G"

    while rows > 0
      cols = 0
      print " "

      while cols <= @cols
        print " " + @board[rows - 1, cols].to_s
        cols += 1
      end

      print "\n"
      rows -= 1
    end
  end

  def put_chip(column, turn)
    row = 0
    while row < @rows
      if @board[row, column] == 0
        @board[row, column] = turn
        return true
      end
      row += 1
    end
    puts "Column is full, try another one."
    return false
  end
  
  def check_four(row, col, row_inc, col_inc)
    connected = 1
    player = @board[row, col]

    col += col_inc
    row += row_inc
    while (col >= 0 && col < @cols && row >= 0 &&
        row < @rows && @board[row, col] == player)
      connected += 1
      if connected == 4
        return true
      end
      col += col_inc
      row += row_inc
    end
    return false
  end
  
  def check_winner(col)
    row = @rows - 1
    incs = [[1, 0], [1, 1], [0, 1], [-1, 0], [-1, -1], [0, -1]]
    x = 0

    while row >= 0 && @board[row, col] == 0
      row -= 1
    end

    while x < 6
      if check_four(row, col, incs[x][0], incs[x][1]) == true
        return true
      end
      x += 1
    end

    return false
  end

end